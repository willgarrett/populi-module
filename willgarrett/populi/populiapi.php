<?php

namespace willgarrett\populi;

require_once(__DIR__."/../../vendor/autoload.php");

        //use \Mashape\Unirest;
//use \Mashape;
class PopuliAPI{

    protected $token;
    protected $uri;

    public function __construct()
    {
            $this->uri=null;
            $this->token=null;
            // NULL
    }
    public function setURI($my_uri)
    {
        $this->uri = $my_uri;

    }
    public function setAccessKey($key)
    {
        $this->token = $key;
    }
    public function login($username, $password)
    {
        $this->token = $this->getAccessKey($username, $password);
    }
    
        // Helper Function, equivalent to loging in and getting the token for a user
    public function getAccessKey($username, $password)
    { 
        $params='username='.urlencode($username)."&password=".urlencode($password);
        
        
        // Place the results into an XML string. We can't use file_get_contents since it randomly fails... so we now use curl
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $this->uri);
        $response = curl_exec($curl);
        curl_close($curl);
        
        if( $response !== false) {
            // Use SimpleXML to put results into Simple XML object (requires PHP5)
            $xml = new \SimpleXMLElement($response);
            if( isset($xml->access_key) ){
                $this->token = (string)$xml->access_key;
            }
            else{
                throw new PopuliException("Oops! Please enter a valid username/password.", 'AUTHENTICATION_ERROR');
            }
        }
        else{
            throw new PopuliException("Oops! We're having trouble connecting to Populi right now... please try again later.", 'CONNECTION_ERROR');
        }

    }
    public function doTask($task, $params = array(), $return_raw = false)
    {
        if( !$this->token ){
            throw new \Exception("Whoops! Please call login before trying to perform a task!");
        }
        
        $post = 'task=' . urlencode($task) . '&access_key=' . $this->token;
        
        foreach($params as $param => $value){
            if( is_array($value) ){
                foreach($value as $array_value){
                    $post .= '&' . $param . '[]=' . urlencode($array_value);
                }
            }
            else{
                $post .= "&$param=" . urlencode($value);
            }
        }
        //echo $post."\n"."<!---------------EOP-------------------/!>";
        //echo "\n\n";
        // Place the results into an XML string. We can't use file_get_contents since it randomly fails... so we now use curl
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $this->uri);
        $response = curl_exec($curl);
        curl_close($curl);
        
        if( $curl !== false ){
            if( $return_raw ){
                return $response;
            }
            else{
                // Use Simple XML to put results into Simple XML object (requires PHP5)
                try{
                    $xml = new \SimpleXMLElement($response);
                }
                catch(Exception $e){
                    echo htmlentities($response) . '<br><br><br>';
                    throw new PopuliException('Problem parsing the XML response: ' . $e->getMessage());
                }
                
                if( $xml->getName() == 'response' ){
                    return $xml;
                }
                else if( $xml->getName() == 'error' ){
                    throw new PopuliException((string)$xml->message, (string)$xml->code);
                }
                else{
                    //Woah - response or error should always be the root element
                    throw new PopuliException('Problem parsing the XML response: invalid root element.');
                }
            }
        }
        else{
            throw new PopuliException('Could not connect to Populi.', 'CONNECTION_ERROR');
        }
    }

}
