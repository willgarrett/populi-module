<?php

namespace willgarrett\populi;

class PopuliException extends \Exception{
    /**************************************************************************************************
     *  We have our own variable since we don't feel like using numeric error codes
     *  Should be one of:
     *      AUTHENTICATION_ERROR - Couldn't login to the API (bad username/password)
     *      BAD_PARAMETER - You called a task using parameters it didn't like
     *      CONNECTION_ERROR - Thrown if we can't connect to Populi 
     *      LOCKED_OUT - Your user account is blocked (too many failed login attempts?)
     *      OTHER_ERROR - Default generic error
     *      PERMISSIONS_ERROR - You aren't allowed to call that task with those parameters
     *      UKNOWN_TASK - You tried to call an API task that doesn't exist
    ****************************************************************************************************/
    public $populi_code = null;
    
    public function __construct($message, $populi_code = 'OTHER_ERROR'){
        parent::__construct($message);
        $this->populi_code = $populi_code;
    }
    
    public function getPopuliCode(){
        return $this->populi_code;
    }
}